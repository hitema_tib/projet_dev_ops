# Utilise une image Python
FROM python:3.9

# Définit le répertoire de travail de l'image
WORKDIR /app

# Copie les fichiers requis dans le conteneur
COPY . /app

# Installe les dépendances
RUN pip install flask

# Expose le port 8080
EXPOSE 8089

# Commande pour démarrer le serveur Python
CMD ["python", "server.py"]
