// Créer le graphique ApexCharts à l'initialisation
let chart;
const selectAnnee = document.getElementById('annee');

function actualiserGraphique() {
  fetch('/parse_csv')
    .then(response => response.json())
    .then(data => {
      const regionsData = {}; 
      const selectedYear = parseInt(selectAnnee.value);
      const regionsOrder = [];

      data.forEach(row => {
        const region = row.nom_region;
        const population = parseInt(row.nombre_d_habitants);
        const annee = parseInt(row.annee_publication);

        if (selectedYear === annee) {
          if (!regionsData[region]) {
            regionsData[region] = 0;
            regionsOrder.push(region);
          }
          regionsData[region] += population;
        }
      });

      const regions = {};
      regionsOrder.sort(); // Tri des noms de régions par ordre alphabétique
      regionsOrder.forEach(region => {
        regions[region] = regionsData[region];
      });

      const labels = Object.keys(regions);
      const series = Object.values(regions);

      // Mettre à jour les données de la série du graphique
      chart.updateSeries([{ data: series }]);

      // Mettre à jour les étiquettes de l'axe x
      chart.updateOptions({
        xaxis: {
          categories: labels,
        },
      });
    })
    .catch(error => {
      console.error('Erreur lors de la récupération des données :', error);
    });
}

selectAnnee.addEventListener('change', actualiserGraphique);
selectAnnee.addEventListener('change', actualiserGraphiqueTauxChomage);
selectAnnee.addEventListener('change', actualiserGraphiqueConstruction);

// Créer le graphique initial
document.addEventListener('DOMContentLoaded', () => {
  chart = new ApexCharts(document.querySelector('#chart'), {
    chart: {
      type: 'bar',
      height: 400,
      width: '100%',
      toolbar: {
        show: true,
        tools: {
          download: true,
        },
      },
    },
    plotOptions: {
        bar: {
          distributed: true, // Applique différentes couleurs à chaque barre
          horizontal: false,
          endingShape: 'rounded',
          columnWidth: '70%',
          dataLabels: {
            position: 'top',
          },
        },
      },
    dataLabels: {
      enabled: true,
      offsetY: -20,
      style: {
        fontSize: '12px',
        colors: ['#304758'],
      },
    },
    legend: {
        show: false, // Cela désactive l'affichage de la légende
    },
    stroke: {
      show: true,
      width: 2,
      colors: ['transparent'],
    },
    xaxis: {
      categories: [], // Initialement, les données de l'axe x sont vides
      title: {
        text: 'Nombre d\'habitants',
      },
    },
    yaxis: {
      title: {
        text: 'Nombre d\'habitants',
      },
    },
    series: [
      {
        name: 'Habitants',
        data: [], // Initialement, les données de la série sont vides
      },
    ],
  });

  chart.render();

  actualiserGraphique();
});


// Créer le graphique ApexCharts pour le taux de chômage
let chartTauxChomage;

function actualiserGraphiqueTauxChomage() {
  fetch('/parse_csv')
    .then(response => response.json())
    .then(data => {
      const regionsDataTauxChomage = {};
      const selectedYear = parseInt(selectAnnee.value);
      const regionsOrder = [];

      data.forEach(row => {
        const region = row.nom_region;
        const tauxChomage = parseFloat(row.taux_de_chomage_au_t4_en);
        const annee = parseInt(row.annee_publication);

        if (selectedYear === annee) {
          if (!regionsDataTauxChomage[region]) {
            regionsDataTauxChomage[region] = 0;
            regionsOrder.push(region);
          }
          regionsDataTauxChomage[region] = tauxChomage;
        }
      });

      const regionsTauxChomage = {};
      regionsOrder.sort(); // Tri des noms de régions par ordre alphabétique
      regionsOrder.forEach(region => {
        regionsTauxChomage[region] = regionsDataTauxChomage[region];
      });

      const labelsTauxChomage = Object.keys(regionsTauxChomage);
      const seriesTauxChomage = Object.values(regionsTauxChomage);

      // Mettre à jour les données de la série du graphique
      chartTauxChomage.updateSeries([{ data: seriesTauxChomage }]);

      // Mettre à jour les étiquettes de l'axe x
      chartTauxChomage.updateOptions({
        xaxis: {
          categories: labelsTauxChomage,
        },
      });
    })
    .catch(error => {
      console.error('Erreur lors de la récupération des données pour le taux de chômage :', error);
    });
}

// Créer le graphique initial pour le taux de chômage
document.addEventListener('DOMContentLoaded', () => {
  chartTauxChomage = new ApexCharts(document.querySelector('#chartTauxChomage'), {
    chart: {
      type: 'bar',
      height: 400,
      width: '100%',
      toolbar: {
        show: true,
        tools: {
          download: true,
        },
      },
    },
    plotOptions: {
        bar: {
          distributed: true, // Applique différentes couleurs à chaque barre
          horizontal: false,
          endingShape: 'rounded',
          columnWidth: '70%',
          dataLabels: {
            position: 'top',
          },
        },
      },
    dataLabels: {
      enabled: true,
      offsetY: -20,
      style: {
        fontSize: '12px',
        colors: ['#304758'],
      },
    },
    legend: {
        show: false, // Cela désactive l'affichage de la légende
    },
    stroke: {
      show: true,
      width: 2,
      colors: ['transparent'],
    },
    xaxis: {
      categories: [], // Initialement, les données de l'axe x sont vides
      title: {
        text: 'Taux de chômage',
      },
    },
    yaxis: {
      title: {
        text: 'Taux de chômage',
      },
    },
    series: [
      {
        name: 'Taux de chômage',
        data: [], // Initialement, les données de la série sont vides
      },
    ],
  });

  chartTauxChomage.render();

  actualiserGraphiqueTauxChomage();
});


// Créer le graphique ApexCharts pour le nombre de constructions par région
let chartConstruction;

function actualiserGraphiqueConstruction() {
  fetch('/parse_csv')
    .then(response => response.json())
    .then(data => {
      const regionsData = {}; 
      const selectedYear = parseInt(selectAnnee.value);
      const regionsOrder = [];

      data.forEach(row => {
        const region = row.nom_region;
        const constructions = parseInt(row.moyenne_annuelle_de_la_construction_neuve_sur_10_ans);
        const annee = parseInt(row.annee_publication);

        if (selectedYear === annee) {
          if (!regionsData[region]) {
            regionsData[region] = 0;
            regionsOrder.push(region);
          }
          regionsData[region] += constructions;
        }
      });

      const regions = {};
      regionsOrder.sort(); // Tri des noms de régions par ordre alphabétique
      regionsOrder.forEach(region => {
        regions[region] = regionsData[region];
      });

      const labels = Object.keys(regions);
      const series = Object.values(regions);

      // Mettre à jour les données de la série du graphique
      chartConstruction.updateSeries([{ data: series }]);

      // Mettre à jour les étiquettes de l'axe x
      chartConstruction.updateOptions({
        xaxis: {
          categories: labels,
        },
      });
    })
    .catch(error => {
      console.error('Erreur lors de la récupération des données pour le nombre de constructions :', error);
    });
}

// Créer le graphique initial pour le nombre de constructions par région
document.addEventListener('DOMContentLoaded', () => {
  chartConstruction = new ApexCharts(document.querySelector('#chartConstructions'), {
    chart: {
      type: 'bar',
      height: 400,
      width: '100%',
      toolbar: {
        show: true,
        tools: {
          download: true,
        },
      },
    },
    plotOptions: {
        bar: {
          distributed: true, // Applique différentes couleurs à chaque barre
          horizontal: false,
          endingShape: 'rounded',
          columnWidth: '55%',
          dataLabels: {
            position: 'top',
          },
        },
      },
    dataLabels: {
      enabled: true,
      offsetY: -20,
      style: {
        fontSize: '12px',
        colors: ['#304758'],
      },
    },
    legend: {
        show: false, // Cela désactive l'affichage de la légende
    },
    stroke: {
      show: true,
      width: 2,
      colors: ['transparent'],
    },
    xaxis: {
      categories: [], // Initialement, les données de l'axe x sont vides
      title: {
        text: 'Nombre de constructions',
      },
    },
    yaxis: {
      title: {
        text: 'Nombre de constructions',
      },
    },
    series: [
      {
        name: 'Constructions',
        data: [], // Initialement, les données de la série sont vides
      },
    ],
  });

  chartConstruction.render();

  actualiserGraphiqueConstruction();
});


const select_table_Annee = document.getElementById('table_annee');
const selectRegions = document.getElementById('regions');
const selectDepartements = document.getElementById('departement');
const selectTauxChomage = document.getElementById('tauxChomage');
const selectTauxChomageSup = document.getElementById('tauxChomagesup');
const selectTauxPauvrete = document.getElementById('tauxPauvrete');
const selectTauxPauvreteSup = document.getElementById('tauxPauvretesup');

fetch('/parse_csv')
  .then(response => response.json())
  .then(data => {
    const uniqueAnnees = [...new Set(data.map(item => item.annee_publication))];
    const uniqueRegions = [...new Set(data.map(item => item.nom_region))];
    const uniqueDepartements = [...new Set(data.map(item => item.code_departement))];
    const uniqueTauxChomage = [...new Set(data.map(item => item.taux_de_chomage_au_t4_en))]; // À adapter au vrai nom de la colonne
    const uniqueTauxPauvrete = [...new Set(data.map(item => item.taux_de_pauvrete_en))]; // À adapter au vrai nom de la colonne

    // Fonction pour remplir un select avec des options
    const fillSelect = (select, values) => {
        // Ajouter une option "tout" en premier
        const optionTout = document.createElement('option');
        optionTout.value = 'tout';
        optionTout.textContent = 'tout';
        select.appendChild(optionTout);
      
        // Trier les valeurs en fonction de leur type (chaîne ou nombre)
        const sortedValues = values.sort((a, b) => {
          if (!isNaN(a) && !isNaN(b)) {
            // Trier comme des nombres
            return a - b;
          } else {
            // Trier comme des chaînes de caractères
            return a.toString().localeCompare(b.toString());
          }
        });
      
        // Créer des options pour chaque valeur triée
        sortedValues.forEach(value => {
          const option = document.createElement('option');
          option.value = value;
          option.textContent = value;
          select.appendChild(option);
        });
      };
      
      

    const fillSelect2 = (select) => {
        const optionTout = document.createElement('option');
        optionTout.value = 'tout';
        optionTout.textContent = 'tout';
        select.appendChild(optionTout);
    
        let percent = 0.0;
        while (percent <= 100.0) {
            const option = document.createElement('option');
            // Utilisation de toFixed pour formater en tant que chaîne avec un chiffre après la virgule
            option.value = percent.toFixed(1);
            option.textContent = percent.toFixed(1) + '%'; // Ajoutez '%' pour la clarté de l'utilisateur
            select.appendChild(option);
            percent += 5.0;
        }
    };
    

    // Remplir les select avec les valeurs uniques
    fillSelect(selectRegions, uniqueRegions);
    fillSelect(selectDepartements, uniqueDepartements);
    fillSelect2(selectTauxChomage, uniqueTauxChomage);
    fillSelect2(selectTauxChomageSup, uniqueTauxChomage);
    fillSelect2(selectTauxPauvrete, uniqueTauxPauvrete);
    fillSelect2(selectTauxPauvreteSup, uniqueTauxPauvrete);
  })
  .catch(error => {
    console.error('Erreur lors de la récupération des données :', error);
  });




  let parsedData;
  // Récupérer l'élément où le tableau Bootstrap sera affiché
const tableContainer = document.getElementById('table-container');


function construireTableau(data) {
    const table = document.createElement('table');
    table.classList.add('table', 'table-striped'); // Ajouter des classes Bootstrap pour le style
    
    if (data.length === 0) {
        data = ["aucune données"];
    }

  // Créer l'en-tête du tableau
  const thead = document.createElement('thead');
  const headers = Object.keys(data[0]); // Supposant que la première ligne du tableau contient les en-têtes
  const headerRow = document.createElement('tr');
  headers.forEach(header => {
    const th = document.createElement('th');
    th.textContent = header;
    headerRow.appendChild(th);
  });
  thead.appendChild(headerRow);
  table.appendChild(thead);

  // Créer le corps du tableau avec les données
  const tbody = document.createElement('tbody');
  data.forEach(rowData => {
    const row = document.createElement('tr');
    headers.forEach(header => {
      const cell = document.createElement('td');
      cell.textContent = rowData[header];
      row.appendChild(cell);
    });
    tbody.appendChild(row);
  });
  table.appendChild(tbody);

  // Ajouter le tableau dans le conteneur
  tableContainer.innerHTML = ''; // Effacer le contenu précédent s'il y en a
  tableContainer.appendChild(table);
}

fetch('/parse_csv') // Modifier l'URL selon votre endpoint
  .then(response => response.json())
  .then(data => {
    parsedData = data; // Stocker les données dans la variable globale
    //construireTableau(data); // Construire le tableau initial
  })
  .catch(error => {
    console.error('Erreur lors de la récupération des données :', error);
  });

// Fonction pour filtrer les données en fonction des critères sélectionnés
function filterData() {
  const selectedAnnee = select_table_Annee.value;
  const selectedRegion = selectRegions.value;
  const selectedDepartement = selectDepartements.value;
  const selectedTauxChomage = selectTauxChomage.value;
  const selectedTauxChomageSup = selectTauxChomageSup.value;
  const selectedTauxPauvrete = selectTauxPauvrete.value;
  const selectedTauxPauvreteSup = selectTauxPauvreteSup.value;

    const filteredData = parsedData.filter(item => {
        const tauxDeChomage = parseFloat(item.taux_de_chomage_au_t4_en);
        const tauxChomageSup = parseFloat(selectedTauxChomageSup);
        const tauxPauvrete = item.taux_de_pauvrete_en !== "" ? parseFloat(item.taux_de_pauvrete_en) : null;

        if (tauxDeChomage >= tauxChomageSup) {
            console.log(tauxDeChomage + " et " + tauxChomageSup);
        }

        return (
        (selectedAnnee === 'tout' || item.annee_publication === selectedAnnee) &&
        (selectedRegion === 'tout' || item.nom_region === selectedRegion) &&
        (selectedDepartement === 'tout' || item.code_departement === selectedDepartement) &&
        (selectedTauxChomage === 'tout' || tauxDeChomage <= selectedTauxChomage) &&
        (selectedTauxChomageSup === 'tout' || tauxDeChomage >= tauxChomageSup) &&
        ((selectedTauxPauvrete === 'tout' || (tauxPauvrete !== null && tauxPauvrete <= selectedTauxPauvrete)) &&
        (selectedTauxPauvreteSup === 'tout' || (tauxPauvrete !== null && tauxPauvrete >= selectedTauxPauvreteSup)))
        );
    });


  construireTableau(filteredData);
}

// Ajouter un écouteur d'événements pour le changement de sélection
select_table_Annee.addEventListener('change', filterData);
selectRegions.addEventListener('change', filterData);
selectDepartements.addEventListener('change', filterData);
selectTauxChomage.addEventListener('change', filterData);
selectTauxChomageSup.addEventListener('change', filterData);
selectTauxPauvrete.addEventListener('change', filterData);
selectTauxPauvreteSup.addEventListener('change', filterData);
  