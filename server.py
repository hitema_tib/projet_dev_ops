import csv
from flask import Flask, render_template, request, jsonify, send_from_directory
import json

app = Flask(__name__)

@app.route('/parse_csv', methods=['GET'])
def parse_csv():
    csv_data = []
    with open('logements.csv', 'r', newline='', encoding='utf-8') as csvfile:
        csv_reader = csv.DictReader(csvfile, delimiter=';')
        for row in csv_reader:
            csv_data.append(row)
    
    return jsonify(csv_data)

@app.route('/')
def index():
    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
